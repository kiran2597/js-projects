import logo from './logo.svg';
import './App.css';
import Coursesales from './Coursesales';

function App() {

  var courses = [
    {name:'C programming',price:200},
    {name:'Java Programming',price:300},
    {name:'Python Programming',price:350}

  ]


  return (
    <div className="App">
      <header className="App-header">
          <h1>Welcome to Course Home page</h1>
      </header>

      <Coursesales items = {courses}/>

    </div>
  );
}

export default App;
