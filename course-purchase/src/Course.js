
import React, { Component } from 'react';

class Course extends Component {


    clicker(sumPrice,itemPrice){
        
        var active = !this.state.active;
        this.setState({ active: active});
        sumPrice(active ? itemPrice : -itemPrice );
        
    }


    constructor(props) {
        super(props);
        
        this.state = {
            active: false
        }
        this.clicker = this.clicker.bind(this);
    }
    
    render() {

        const {name,price,key,sumPrice} = this.props;
        return (
            <div>
                <p className={this.state.active ? 'active' : ''} onClick={() =>this.clicker(sumPrice,price)} key={key}>{name} <b>{price}</b></p>
            </div>
        );
    }
}

export default Course;