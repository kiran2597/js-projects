

import React, { Component} from 'react';
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyA4J6NGGGQKt6KhTKZHGrreyQDNs3cyOaI",
    authDomain: "authentication-c2fe1.firebaseapp.com",
    databaseURL: "https://authentication-c2fe1-default-rtdb.firebaseio.com",
    projectId: "authentication-c2fe1",
    storageBucket: "authentication-c2fe1.appspot.com",
    messagingSenderId: "741784356124",
    appId: "1:741784356124:web:3f91c0cf33426cdbf226d9"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);



class Authentic extends Component {


    login(event){

        event.preventDefault();
        
        const {email,password} = this.state;
        const auth = firebase.auth();
        const promise = auth.signInWithEmailAndPassword(email,password);

        promise
            .then( user =>{
                var logout = document.getElementById('logout');
                logout.classList.remove('hide');

                this.setState({
                    message : `Welcome user, ${user.email}`
                })
            })
            .catch(e => {
            const error = e.message;
            console.log(error);

            this.setState({ message: error});
        });
    }

    handleChange= (e) =>{

        const {name,value} = e.target;

        this.setState({
            ...this.state,
            [name] : value
        })

    }

    signup(){

        const {email,password} = this.state;

        const auth = firebase.auth();
        const promise = auth.createUserWithEmailAndPassword(email,password);

        promise
              .then(user =>{
                  var msg = "Welcome user, "+ user.email;
                  
                  firebase.database().ref('users/'+user.uid).set({
                      email: user.email
                  });
                  console.log(user);
                  this.setState({ message:msg});
              })
              .catch(e =>{
                  var err_msg = e.message;
                  console.log(err_msg);
                  this.setState({
                      message : err_msg
                  })
              })

    }

    logout(){
        firebase.auth().signOut();
        var logout = document.getElementById('logout');
        logout.classList.add('hide');

        this.setState({
            email: '',
            password:'',
            message: `Thank you`
        })

    }

    signInGoogle(){



    }

    constructor(props) {
        super(props);
        
        this.state = {
            email: '',
            password: '',
            message: ''
        }
        this.login = this.login.bind(this);
        this.signup = this.signup.bind(this);
        this.logout = this.logout.bind(this);
        this.signInGoogle = this.signInGoogle.bind(this);
    }


    render() {
        return (
            <div>
                <input type="email" placeholder='Enter your email' name='email' value={this.state.email} onChange={this.handleChange}/><br/>
                <input type="password" placeholder='Enter your password' name='password' value={this.state.password} onChange={this.handleChange}/><br/>

                <p>{this.state.message}</p>
                <button onClick={this.login}>Log In</button>
                <button onClick={this.signup}>Sign Up</button>
                <button id="logout" className='hide' onClick={this.logout}>Log Out</button>
                <button id="google" onClick={this.signInGoogle}>Sign in Google</button>
            </div>
        );
    }
}


export default Authentic;