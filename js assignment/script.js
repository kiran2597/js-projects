

(function(){

    const images=['slides/image1.jpg',"slides/image2.jpg",'slides/image3.jpg','slides/image4.jpg','slides/image5.jpg']
    let currentImage =0;

    const prevBtn = document.getElementById('previous');
    const nextBtn = document.getElementById('next');
    const container  = document.getElementById('content');

    nextBtn.addEventListener('click',function(event){

        event.preventDefault();
        currentImage++;
        if(currentImage > images.length-1)
        {
            currentImage = 0;
        }
        newImage();
    });

    prevBtn.addEventListener('click',function(event){
        
        event.preventDefault();
        currentImage--;
        if(currentImage <0)
        {
            currentImage = images.length-1;
        }
        newImage();
    });

    function newImage(){

        var newSlide = document.createElement('img');
            newSlide.src = `${images[currentImage]}`;
            newSlide.className = 'fadeinimg';
            container.appendChild(newSlide);
    
            if(container.children.length > 2){
                container.removeChild(container.children[0]);
            }
    }
}());
