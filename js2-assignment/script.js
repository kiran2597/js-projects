

document.getElementById('convert').addEventListener('submit',function(evt){

    evt.preventDefault();

    var distance = parseFloat(document.getElementById('distance').value);
    var answer = document.getElementById('answer');
    if(distance){

        var value = distance * 1.689344;
        var conversion = value.toFixed(3);
        
        answer.innerHTML = `<h2>${distance} miles converts to ${conversion} kilometers.</h2>`;
    }
    else{
        answer.innerHTML = '<h2>Oops! you have not entered a number.</h2>'
    }


});