

let convertType = "miles";
const heading = document.querySelector('h1');
const intro = document.querySelector('p');
const form = document.getElementById('convert');


document.addEventListener('keydown',function(event){


    let key = event.code;
    if(key === 'KeyK'){
        convertType = 'kilometers';
        heading.innerHTML = 'Kilometers to Miles converter';
        intro.innerHTML = 'Type in a number of kilometers and click the button to convert the distance to miles.'   
    }
    else if(key === 'KeyM'){
        convertType = 'miles';
        heading.innerHTML = 'Miles to Kilometers converter';
        intro.innerHTML = 'Type in a number of miles and click the button to convert the distance to kilometers.' 
    }
});

form.addEventListener('submit',function(event){


    event.preventDefault();
    let distance = parseFloat(document.getElementById('distance').value);
    const answer  = document.getElementById('answer');

    if(distance){

        if(convertType == 'miles'){

            let conversion = (distance * 1.689344).toFixed(3);
            answer.innerHTML = `<h2>${distance} miles converted to ${conversion} kilometers</h2>`;
        }
        else if(convertType == 'kilometers'){
            let conversion = (distance * 0.621371192).toFixed(3);
            answer.innerHTML = `<h2>${distance} kilometers converted to ${conversion} miles</h2>`;
        }
    }
    else{

        answer.innerHTML = '<h2>Oops! Please enter a number </h2>'
    }

});