
//const destinations = [];

const detailsform = document.getElementById('destination_details_form');
detailsform.addEventListener('submit',handleFormSubmit);

function handleFormSubmit(event){

    event.preventDefault();
    let field = event.target;


    let destName = field.elements['name'].value;
    let destLocation = field.elements['location'].value;
    let destphoto = field.elements['photo'].value;
    let destDesc = field.elements['description'].value;

    // const descObj = new Destination(destName,destLocation,destphoto,destDesc);
    // destinations.push(descObj);

    for(let i=0;i<detailsform.length;i++){
        detailsform.elements[i].value = "";
    }


    var destcard = createDestinationCard(destName,destLocation,destphoto,destDesc);

    let wishListContainer = document.querySelector('#destination_container');
    if(wishListContainer.children.length === 0){
        let title = document.createElement('h2');
        title.innerHTML = "My WishList";
        wishListContainer.appendChild(title);
    }
    
    // destinations.push(destcard);
    // console.log(destinations);

    wishListContainer.appendChild(destcard);
}

function createDestinationCard(name,location,photo,description){


    let card = document.createElement('div');
    card.className = "card";

    let img = document.createElement('img');
    img.setAttribute('alt',name);

    let constantPhotoUrl = "images/signpost.jpg";

    if(photo.length === 0){
        img.setAttribute('src',constantPhotoUrl);
    }
    else{
        img.setAttribute('src',photo);
    }
    card.appendChild(img);

    let cardBody = document.createElement('div');
    cardBody.className = "card-body";

    let cardTitle  = document.createElement('h3');
    cardTitle.innerText = name;
    cardBody.appendChild(cardTitle);

    let cardSubTitle  = document.createElement('h4');
    cardSubTitle.innerText = location;
    cardBody.appendChild(cardSubTitle);

    if(description.length !== 0){
        let cardText= document.createElement('p');
        cardText.className = "card-text";
        cardText.innerText = description;
        cardBody.appendChild(cardText);
    }

    let cardDeleteBtn = document.createElement('button');
    cardDeleteBtn.innerText = "Remove";

    let cardEditBtn = document.createElement('button');
    cardEditBtn.innerText = "Edit";

    cardDeleteBtn.addEventListener('click',removeDestination);
    cardEditBtn.addEventListener('click',editDestination);

    cardBody.appendChild(cardDeleteBtn);
    cardBody.appendChild(cardEditBtn);
    card.appendChild(cardBody);

    return card;
}

function removeDestination(event){

    let card = event.target.parentElement.parentElement;
    card.remove();
 
}

function editDestination(event){


    let card = document.createElement('div');
    card.className = "card";

    let title = document.createElement('h3');
    title.innerText = "Update Details";

    let form = document.createElement('form');
    form.setAttribute('id',"editForm");

    let destLabel = document.createElement('label');
    destLabel.innerText = "Name";

    let nameText = document.createElement('input');
    nameText.setAttribute('type',"text");
    nameText.setAttribute('id',"editName");
    nameText.setAttribute('placeholder',"Enter Destination Name");


    let destLocation = document.createElement('label');
    destLocation.innerText = "Location";

    let location= document.createElement('input');
    location.setAttribute('type',"text");
    location.setAttribute('id',"editLocation");
    location.setAttribute('placeholder',"Enter Destination Location");


    let destPhoto = document.createElement('label');
    destPhoto.innerHTML = "Photo Url";

    let photoUrl = document.createElement('input');
    photoUrl.setAttribute("type","url");
    photoUrl.setAttribute('id','editPhoto');
    photoUrl.setAttribute('placeholder','Enter destination Photo Url');


    let destTextarea = document.createElement('label');
    destTextarea.innerText = "Description";

    let textarea = document.createElement('textarea');
    textarea.setAttribute('id','editDescription');
    textarea.setAttribute('placeholder','Enter destination description');

    let updateButton = document.createElement('button');
    updateButton.innerText = "Update Details";
    updateButton.setAttribute('type',"submit");
    updateButton.setAttribute('style','width:100%');

    form.addEventListener('submit',updateDetailsDestination);

    form.appendChild(destLabel);
    form.appendChild(nameText);
    form.appendChild(destLocation);
    form.appendChild(location);
    form.appendChild(destPhoto);
    form.appendChild(photoUrl);
    form.appendChild(destTextarea);
    form.appendChild(textarea);
    form.appendChild(updateButton);

    card.appendChild(title);
    card.appendChild(form);
    

    let existCard = event.target.parentElement.parentElement;

    // const existDestination = new Destination(nameText.getElementById('editName'),location.getElementById('editLocation'),photoUrl.getElementById('editPhoto'),textarea.getElementById('editDescription'));
    // let result = checkData(destinations,existDestination);
    // if(!result){
    //     let tag = document.createElement('h3');
    //     tag.innerHTML = "There is no data ";
    // }

    
    existCard.replaceWith(card);

}

function updateDetailsDestination(event){
        event.preventDefault();

        let editForm = event.target.parentElement
        editForm.remove();

        let destName = event.target.elements['editName'].value;
        let destLocation = event.target.elements['editLocation'].value;
        let destPhoto = event.target.elements['editPhoto'].value;
        let destDesc = event.target.elements['editDescription'].value;

        let updateWishListCard = createDestinationCard(destName,destLocation,destPhoto,destDesc);
        console.log(updateWishListCard);
        let wishListContainer = document.getElementById('destination_container');
        wishListContainer.appendChild(updateWishListCard);

}
