import logo from './logo.svg';
import './App.css';
import {Component} from 'react';

function App() {
  return (
    <div className="App" align="center">
      <header className="App-header">
        
      </header>
      <Parent/>
    </div>
  );
}

class Parent extends Component{

    render(){
      return(
          <div>
            <h2>Just Some Info</h2>
            <Child msg={"This is cars component"} cars={this.props.cars}/>
          </div>
      );
    }
}

Parent.defaultProps = {
   cars : ['BMW','MERC','CITY','AUDI']
}  


class Child extends Component {

    render(){
      return(
        <div>
          <h3>{this.props.msg}</h3>
          {this.props.cars.map((item,i)=>{
            return <p key={i}>{item}</p>
          })}
        </div>
      );
    }
}


export default App;
