
import React, { useState } from 'react';
import '../App.css';


function Slides ({slides}){

    const [activeSlide,setActiveSlide] = useState(slides[0]);
    const [currentSlide,setCurrentSlide] = useState(0);
    const [nextBtn,setNextBtn] = useState(false);
    const [prevBtn,setPrevBtn] = useState(true);
    const [restartBtn,setRestartBtn] = useState(true);
    

    const clickNextBtn = () =>{

        let slide = currentSlide;

        if(currentSlide < slides.length -1){
            slide++;
            setRestartBtn(false);
            setPrevBtn(false);
            setCurrentSlide(slide);
            setActiveSlide(slides[slide]);
        }
        
        if(slide === slides.length - 1){
            setNextBtn(true);
        }
        
    }

    const clickPrevBtn = () =>{

        let slide = currentSlide;
        
        if( slide > 0){
            slide--;
            setCurrentSlide(slide);
            setActiveSlide(slides[slide]);
            setNextBtn(false);
        }

        if(slide === 0){
            setPrevBtn(true);
            setRestartBtn(true);
        }
    }

    const clickRestartBtn = () =>{

        setCurrentSlide(0);
        setActiveSlide(slides[0]);
        setPrevBtn(true);
        setRestartBtn(true);
        setNextBtn(false);
    }


    return (
        <div>
            <div id="navigation" className="text-center">
                <button data-testid="button-restart" className="small outlined" onClick = {clickRestartBtn} disabled={restartBtn}>Restart</button>
                <button data-testid="button-prev" className="small" onClick = {clickPrevBtn} disabled={prevBtn}>Prev</button>
                <button data-testid="button-next" className="small" onClick={clickNextBtn} disabled={nextBtn}>Next</button>
            </div>
            <div id="slide" className="card text-center">
                <h1 data-testid="title">{activeSlide.title}</h1>
                <p data-testid="text">{activeSlide.text}</p>
            </div>
        </div>
    );
}

export default Slides;