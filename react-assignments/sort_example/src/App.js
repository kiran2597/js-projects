import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';

function App() {

  const names=[
      {
        firstname:"kiran",
        lastname:"chenna"
      },
      {
        firstname:"chandra",
        lastname:"satya"
      },{
        firstname:"khagesh",
        lastname:"chapa"
      },{
        firstname:"rajesh",
        lastname:"gujjala"
      }
  ]

  const [list,setList] = useState(names);

  useEffect(() =>{
      sortByLastName();
  },[]);



  const sortByFirstName = () =>{
      let sortByName = [];

      Object.assign(sortByName,list);
      sortByName.sort( (a,b) => {
         if(a.firstname > b.firstname)
            return 1;
         else if(a.firstname < b.firstname)
            return -1;
         return 0;
      });
      setList(sortByName);
  }

  const sortByLastName = () =>{
     
      let sortByName = [];

      Object.assign(sortByName,list);
      sortByName.sort( (a,b) => {
        if(a.lastname > b.lastname)
            return 1;
        else if(a.lastname < b.lastname)
            return -1;
        return 0;
      });
      setList(sortByName);
  }

  return (
    <div className="App">
      <div className="layout-row align-items-center justify-content-center my-20 navigation">
                <button data-testid="most-upvoted-link" className="small" onClick={() => sortByFirstName()}>Most Upvoted</button>
                <button data-testid="most-recent-link" className="small" onClick={() => sortByLastName()}>Most Recent</button>
      </div>
      <table align="center">
        <thead>
          <tr>
            <th>FirstName</th>
            <th>LastName</th>
          </tr>
        </thead>
        <tbody>
           {list.map((k,index) => (
              <tr key={index}>
                  <td>{k.firstname}</td>
                  <td>{k.lastname}</td>
              </tr>
           ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
