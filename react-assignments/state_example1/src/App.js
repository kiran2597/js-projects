import logo from './logo.svg';
import './App.css';
import React,{ useState } from 'react';


function Panel({title,children,active,show}){

    return (
      <>
        <h3>{title}</h3>
        { active ? (
          <p>{children}</p>
        ):(
          <button onClick={show}>Show</button>
        )}
      </>
    );
}



function App() {

  const [isActive,setIsActive] = useState(0); 
  return (
    <>
      <h2>Almaty, Kazakhstan</h2>
      <Panel title="About" active = {isActive === 0} show = {() => setIsActive(0)}>
        With a population of about 2 million, Almaty is Kazakhstan's largest city. From 1929 to 1997, it was its capital city.
      </Panel>
      <Panel title="Etymology" active = {isActive === 1} show = {() => setIsActive(1)}>
        The name comes from <span lang="kk-KZ">алма</span>, the Kazakh word for "apple" and is often translated as "full of apples". In fact, the region surrounding Almaty is thought to be the ancestral home of the apple, and the wild <i lang="la">Malus sieversii</i> is considered a likely candidate for the ancestor of the modern domestic apple.
      </Panel>
    </>
  );
}

export default App;
