import { useState } from "react";
import './common.css'



function Counter(){

    const [hover,setHover] = useState(false);
    const [score,setScore] = useState(0);

    
    let className = 'counter';
    if (hover) {
        className += ' hover';
    }

    return(
        <div className={className}
             onPointerEnter = {() => setHover(true)}
             onPointerLeave  = {() => setHover(false)}
        >
            <h1>{score}</h1>
            <button onClick={()=> setScore(score+1)}>Add One</button>
        </div>
    );
}


export default function App1(){

    const [showNxt,setShowNxt] = useState(false);
   
    return(
        <>
          <Counter/>
          {
              showNxt && <Counter/>
          }
          <label>
              <input
                type="checkbox"
                checked={showNxt}
                onChange = {(e)=>setShowNxt(e.target.checked)}
              />
              Render Second Input
          </label>
        </>
    );
}

