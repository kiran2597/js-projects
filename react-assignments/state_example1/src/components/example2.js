import { useState } from "react";
import './common.css'

export default function ScoreBoard(){

    const [player,setPlayer] = useState(false);

    return(
        <div>
            { player &&    <Counter person="Kiran"/> }
            { !player &&   <Counter person="Kumar"/> }
            <button onClick={() => setPlayer(!player)}>
                Next Player
            </button>
        </div>
    );
}

function Counter({person}){

    const [hover,setHover] = useState(false);
    const [score,setScore] = useState(0);
    
    let className = 'counter';
    if (hover) {
        className += ' hover';
    }

    return(
        <div className={className}
             onPointerEnter = {() => setHover(true)}
             onPointerLeave  = {() => setHover(false)}>

            <h1>{person}'s score: {score}</h1>
            <button onClick={ () => setScore(score+1)}>Add One</button>
        </div>
    );
}