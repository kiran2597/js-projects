import { useState } from "react";



export default function SwapForms(){

    const [reverse,setReverse] = useState(false);
    const [text,setText] = useState('');


    let checkbox = (

        <label>
            <input type="checkbox"
                value={reverse}
                onChange={(e) => setReverse(e.target.checked)}
            />
            Reverse Order
        </label>
    );

     if(reverse){
            return(
                <>
                    <Field key="lastname" label="LastName"/>
                    <Field key="firstname" label="FirstName"/>
                    {checkbox}
                </>
            );
    }
    else{
        return(
            <>
                <Field key="firstname" label="FirstName"/>
                <Field key="lastname" label="LastName"/>
                {checkbox}
            </>
        );
    }
}



function Field({label}){

    const [text,setText] = useState('');
    return(
        <>
            <label>{label} : {' '}</label>
            <input 
                type="text" 
                value={text}
                onChange = { (event) =>setText(event.target.value)}
                />
        </>
    );
}