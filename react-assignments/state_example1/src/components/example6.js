
import { useState } from 'react';


export  default function ContactList(){

    const [reverse,setReverse] = useState(false);

    const displayContacts = [...contacts]
    if(reverse){
        displayContacts.reverse();
    }

    return (
        <div>
            <label>
                <input
                    type="checkbox"
                    value={reverse}
                    onChange={ (e) =>setReverse(e.target.checked)}
                />
                {' '}
                Reverse Order
            </label>
            { displayContacts.map((contact) => 
                    <li key={contact.id}>
                        <Contact contact={contact}/>
                    </li>
                )
            }
        </div>
    );
}

function Contact({contact}){

    const [expanded,setExpanded] = useState(false);

    return(
        <>
            <p><i>{contact.name}</i></p> 
            {expanded && (
            <div>
                <p><i>{contact.email}</i></p>
            </div>
            )}
            <button onClick={ ()=>setExpanded( !expanded)}>{!expanded ? 'Show' : 'Hide'} Email</button>
        </>
    );
}

const contacts = [
  { id: 0, name: 'Alice', email: 'alice@mail.com' },
  { id: 1, name: 'Bob', email: 'bob@mail.com' },
  { id: 2, name: 'Taylor', email: 'taylor@mail.com' }
];

