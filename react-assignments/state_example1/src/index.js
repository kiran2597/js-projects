import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import App1 from './components/example1';
import ScoreBoard from './components/example2';
import FormExample from './components/example3';
import SwapForms from './components/example4';
import Gallery from './components/example5';
import ContactList from './components/example6';

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <App1/> */}
    {/* <ScoreBoard/> */}
    {/* <FormExample/> */}
    {/* <SwapForms/> */}
    {/* <Gallery/> */}
    <ContactList/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
