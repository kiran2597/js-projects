import { useState } from "react";

function AddTask({taskAddOn}) {


    const [text,setText] = useState('');
    return(
        <>
            <input 
                type="text" 
                value={text}
                onChange = { (evt) => setText(evt.target.value)} />
            <button onClick={() => {
                setText('');
                taskAddOn(text);
                }
            }>Add</button>
        </>
    )
}
export default AddTask;