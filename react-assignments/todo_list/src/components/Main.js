
import React, { useReducer } from 'react';
import AddTask from './AddTask';
import TaskList from './TaskList';
import taskReducer from './taskReducer';

export default function Main(){

    const [tasks,dispatch] = useReducer(taskReducer,initialTasks);

    const handleTask = (text) =>{
        dispatch({
            type: 'added',
            id: nextId++,
            text: text
        })
    }

    const changeTaskDetails = (task) =>{

        dispatch({
            type: 'changed',
            task: task,
        })
    }


    const deleteTaskDetails = (taskId) =>{

       dispatch({
           type: 'deleted',
           id: taskId
       })
    }


    return(
        <div align="center">
            <h1>Todo List</h1>
            <AddTask taskAddOn = {handleTask}/>
            <TaskList tasks={tasks} onChangeTask={changeTaskDetails} onDeleteTask={deleteTaskDetails}/>
        </div>
    );
} 

let nextId = 3;
const initialTasks = [
  { id: 0, text: 'Visit Kafka Museum', done: true },
  { id: 1, text: 'Watch a puppet show', done: false },
  { id: 2, text: 'Lennon Wall pic', done: false },
];