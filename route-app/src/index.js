import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Router,Route} from 'react-router';
import { browserHistory } from 'react-router';
import One from './One';
import Two from './Two';
import ErrorPage from './ErrorPage';



ReactDOM.render(
  
  <Router history={browserHistory}>
    <Route path='/' component={App}></Route>
    <Route path='/one' component={One}></Route>
    <Route path='/two' component={Two}></Route>
    <Route path='*' component={ErrorPage}></Route>
  </Router>
  , 
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
